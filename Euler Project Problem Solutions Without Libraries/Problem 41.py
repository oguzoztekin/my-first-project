#Problem 41

def is_prime(n): #Prime Tester Function
    if n <= 1:
        return False
    if n <= 3:
        return True
    if n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True

def is_pandigital(n):
  digits = [int(i) for i in list(str(n))] #
  numbers = [i for i in range(1, len(list(str(n))) + 1) if len(list(str(n))) < 10]
  if sorted(digits) == numbers:
    return True

#We can easily see that there isn't any pandigital number with 8 or 9 digits
#(1) If the sum of the digits of a number is divisible by 9, that number is also divisible by 9.
#(2) The sum of the numbers 1 to n is n*(n+1)/2 (Gaussian Sum)
#Proof: For any 8-digit or 9-digit pandigital number, the sum of the digits is divisible by 9
#Thus we can start with looking at the largest possible pandigital number which is 9876543

for a in range(9876544, 0, -1):
  if is_prime(a) and is_pandigital(a):
    print(a)
    break
    